import 'package:flutter/painting.dart';

class Constantes {

  //Mensajes
  static final String title_login = "Bienvenido";
  static final String title_login_button = "Login";
  static final String field_empty = "El campo está vacío";
  static final String labelEmail = "Ingrese Email";
  static final String labelPassword = "Ingrese Password";
  
  //API REST
  static final String pathApi = "http://localhost:8989/biblioteca";
  //static final String pathApi = "http://10.0.2.2:8989/biblioteca";
  //static final String pathApi = "http://ec2-18-218-248-228.us-east-2.compute.amazonaws.com:8080/biblioteca";

  //Operaciones
  static final String operacionLogin = "LOGIN";

  //Respuesta http
  static final int codigoHttpExitoso = 200;
}