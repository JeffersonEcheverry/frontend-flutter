import 'dart:async';
import 'dart:convert';

import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:http/http.dart';
import 'package:localstorage/localstorage.dart';
import 'package:seminario/Administrador/Modelo/global.dart';
import 'package:seminario/Administrador/Modelo/request.dart';
import 'package:seminario/Administrador/Modelo/usuario.dart';
import 'package:seminario/Administrador/Repositorio/api_rest.dart';
import 'package:seminario/constantes.dart';

class LoginBloc extends Bloc {

  //Request Login
  RequestApi requestApi = RequestApi();
  //Objeto Global, este contiene el request
  Global request = Global();
  //LocalStorage
  final LocalStorage storage = LocalStorage('data_storage');

  StreamController _streamController = new StreamController<Response>();
  Response response;
 
  void getUsuario(Usuario usuario) async {
    requestApi.setOperacion(Constantes.operacionLogin);
    requestApi.setBody(usuario);
    request.setRequest(requestApi);
    this.response = await ApiRest().login(jsonEncode(request));
    print("Ejecuto la ApiRest");

    if(response.body != null) {
      print(response.statusCode);
      print(response.body);
      storage.setItem("token", response.body);
       _streamController.add(this.response);
    } 

  }

  Stream<Response> get autenticacion => _streamController.stream;

  @override
  void dispose() {
    // TODO: implement dispose
  }

}