import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:seminario/constantes.dart';

class ApiRest {

  final cliente = http.Client();

  Future<http.Response> login(String request) async{
    print("${Constantes.pathApi}/loginUser/login");
    print(request);
    try{
      var response = await cliente.post("${Constantes.pathApi}/loginUser/login", headers: {HttpHeaders.contentTypeHeader: "application/json"}, body: request);
      if(response.statusCode == 200){
        return response;
      }else{
        return null;
      }
    }finally{
      cliente.close();
    }
  }

}