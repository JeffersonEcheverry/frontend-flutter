import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:seminario/Administrador/BLoc/login_bloc.dart';
import 'package:seminario/Administrador/Modelo/usuario.dart';
import 'package:seminario/Widgets/button_general.dart';
import 'package:seminario/Widgets/menu.dart';
import 'package:seminario/constantes.dart';

class FormularioLogin extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FormularioLogin();
  }
  
}

class _FormularioLogin extends State<FormularioLogin> {
  
  //Identificara de manera unica el formulario
  final _formKey = GlobalKey<FormState>();

  LoginBloc loginBloc = LoginBloc();
  final _codigo = TextEditingController();
  final _password = TextEditingController();

   Widget logeo()  {
     print("*********JSE********");
     print("Ejecuto el botón");
     print("Formulario valido ${_formKey.currentState.validate()}");
     print("El codgio es ${_codigo.text}");
     print("El password es ${_password.text}");
     if(_formKey.currentState.validate()){
        Usuario usuario = Usuario(_codigo.text, _password.text);
        loginBloc.getUsuario(usuario);
        return StreamBuilder(
          stream: loginBloc.autenticacion,
          builder: (BuildContext context, AsyncSnapshot snapshot){
            print("Estoy dentro del stream");
            print(snapshot.data);
            if(snapshot.hasData && !snapshot.hasError){
              return Menu();
            }else{
              return FormularioLogin();
            }
          },
        );
        /*
        print(loginSuccessful);
        if(loginSuccessful){
          print("Login exitoso");
          return Menu();
        }
        */
     }
     return FormularioLogin();
  }

  Widget formulario(){
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: _codigo,
            decoration: InputDecoration(
              icon: Icon(
                Icons.email,
              ),
              labelText: Constantes.labelEmail,
            ),
            validator: (value) {
              if(value.isEmpty) {
                return Constantes.field_empty;
              }
            },
          ),
          TextFormField(
            controller: _password,
            decoration: InputDecoration(
              icon: Icon(
                Icons.https
              ),
              labelText: Constantes.labelPassword,
            ),
            validator: (value) {
              if(value.isEmpty){
                return Constantes.field_empty;
              }
            },
          ),
          Center(
            child: ButtonGeneral(
              title: Constantes.title_login_button, 
              action: logeo, 
              form: _formKey, 
              height: 35.0, 
              width: 250.0
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    loginBloc = BlocProvider.of(context);
    return Container(
      width: 350,
      margin: EdgeInsets.only(
        top: 60,
      ),
      child: formulario(),
    );
  }
  
}