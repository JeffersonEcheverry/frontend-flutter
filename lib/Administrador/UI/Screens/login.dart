import 'package:flutter/material.dart';
import 'package:seminario/Administrador/UI/Widgets/formulario_login.dart';
import 'package:seminario/Widgets/gradient.dart';
import 'package:seminario/Widgets/imagen.dart';
import 'package:seminario/constantes.dart';

class Login extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          GradientBack(height: null, title: ""),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Imagen(heightImage: 200.0, withImage: 200.0, marginRight: 20.0, marginTop: 5.0, withDecoration: 20.0,ulrImage: "assets/img/logo.jpeg"),
              FormularioLogin()
            ],
          ),
        ],
      ),
    );
  }
  
}

