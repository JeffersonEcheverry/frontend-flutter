
class RequestApi {
  
  String operacion;
  Object body;

  RequestApi();

  String get getOperacion => this.operacion;

  void setOperacion(String operacion) {
    this.operacion = operacion;
  }

  Object get getBody => this.body;

  void setBody(Object body){
    this.body = body;
  }

  RequestApi.fromJson(Map<String, dynamic> json)
   : operacion = json['operacion'],
     body = json['body'];

  Map<String, dynamic> toJson() => 
  {
    'operacion': this.operacion,
    'body': this.body
  };

}