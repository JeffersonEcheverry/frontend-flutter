
class Global {

  Object request;

  Global();

  Object get getRequest => this.request;

  void setRequest(Object request){
    this.request = request;
  }

  Global.fromJson(Map<String, dynamic> json)
  : request = json['request'];


  Map<String, dynamic> toJson() => 
  {
    'request': this.request
  };

}