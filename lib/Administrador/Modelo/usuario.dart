
class Usuario {

  //final String email;
  //final String password;
  String codigo;
  String password;

  //getters and setters
  String get getCodigo => this.codigo;

  void setCodigo(String codigo){
    codigo = codigo;
  }

  String get getPassword => this.password;

  void setPassword(String password){
    password = password;
  }

  //Metodos
  Usuario(this.codigo, this.password);

  Usuario.fromJson(Map<String, dynamic> json) 
    : codigo = json['codigo'],
      password = json['password'];

  Map<String, dynamic> toJson() => 
  {
    'codigo': this.codigo,
    'password': this.password
  };

}