import 'package:flutter/material.dart';

class Imagen extends StatelessWidget{
  
  double withImage = 100.0;
  double heightImage = 100.0;
  double marginRight = 60.0;
  double marginTop = 60.0;
  double withDecoration = 20.0;
  String ulrImage = "";

  Imagen({Key key, @required this.withImage, @required this.heightImage, @required this.marginRight, @required this.marginTop, @required this.withDecoration, @required this.ulrImage});
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: withImage,
      height: heightImage,
      margin: EdgeInsets.only(
        right: marginRight,
        top: marginTop,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white,
          width: withDecoration,
          style: BorderStyle.solid
        ),
        shape: BoxShape.circle,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(
            ulrImage
          )
        )
      ),
    );
  }
  
}