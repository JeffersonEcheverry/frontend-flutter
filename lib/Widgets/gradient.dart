import 'package:flutter/material.dart';

class GradientBack extends StatelessWidget{
  
  double height = 0.0;
  String title = "";

  GradientBack({Key key, @required this.height, @required this.title});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xFF2b5876),
            Color(0xFF424376),
          ],
          begin: FractionalOffset(0.5, 0.3),
          end: FractionalOffset(0.5, 1.5),
          stops: [0.2, 0.6],
          tileMode: TileMode.clamp
        ),
      ),
      child: Text(
          title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 30.0,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      alignment: Alignment(-0.9, -0.6),
    );
  }
  
}