import 'package:flutter/material.dart';

class ButtonGeneral extends StatefulWidget {
  
  String title;
  VoidCallback action;
  double height = 30.0;
  double width = 300.0;
  GlobalKey<FormState> form;

  ButtonGeneral({Key key, @required this.title, @required this.action, @required form,this.height, this.width});
  
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ButtonGeneral();
  }
  
}

class _ButtonGeneral extends State<ButtonGeneral> {
  
  
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: widget.action,
      child: Container(
        margin: EdgeInsets.only(
          top: 20.0,
          left: 20.0,
          right: 20.0,
        ),
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          gradient: LinearGradient(
            colors: [
              Color(0xFF373b44),
              Color(0xFF4286f4)
            ],
            begin: FractionalOffset(0.2, 0.0),
            end: FractionalOffset(1.0, 0.6),
            stops: [0.0, 0.6],
            tileMode: TileMode.clamp
          ),
        ),
        child: Center(
          child: Text(
            widget.title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.bold
            ),
          ),
        )
      ),
    );
  }
  
}